<?php

$config = array();

$config['view']['theme'] = 'default';

/*
* Главная
*/

// Включить выбор темы
$config['view']['switcher']['themes'] = true; // true, false

// Включить слайдер
$config['view']['topic']['slider'] = true; // true, false

// Тема слайдера
$config['view']['topic']['slider_theme'] = 'tm-1'; // tm-1, tm-2, tm-3

// Включить сайдбар
$config['view']['sidebar']['index'] = true; // true, false // пока работает не правильно, требует правки размеров топиков

// Включить карусель
$config['view']['carousel']['index'] = true; // true, false 

// Сколько топиков выводить в карусель
$config['view']['carousel']['topic_n'] = 10; // Рекомендуется не меньше трех

// Что выводить в карусель
$config['view']['carousel']['type'] = 'blog'; // Новые топики - new, Топ пользователей - people, Из блога = blog
$config['view']['carousel']['blog_n'] = 1; // Если blog, указываем ID нужного блога. Посмотреть ID можно через phpmyadmin.

/*
* Топики
*/

// Тема топиков
$config['view']['topic']['theme'] = 'tm-1'; // tm-1, tm-2, tm-3, tm-4, tm-5


/*
* Пользователи
*/

$config['module']['user']['avatar_size'] = array(385,100,64,48,24,0); // Список размеров аватаров у пользователя. 0 - исходный размер

/*
* Блоги
*/
$config['module']['blog']['avatar_size'] = array(870,100,64,48,24,0); // Список размеров аватаров у блога. 0 - исходный размер


$config['head']['default']['js'] = Config::Get('head.default.js');
$config['head']['default']['js'][] = '___path.static.skin___/js/ls.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/plugins/nivo-slider/jquery.carouFredSel-6.2.1.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/plugins/nivo-slider/jquery.mousewheel.min.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/plugins/nivo-slider/jquery.nivo.slider.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/plugins/nivo-slider/jquery.touchSwipe.min.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/plugins/slidebars.js';
$config['head']['default']['js'][] = '___path.static.skin___/js/main.js';

$config['head']['default']['css'] = array(
	"___path.static.skin___/css/layout.css",
	"___path.static.skin___/css/base.css",
	"___path.static.skin___/css/fonts.css" => array('merge'=>false),
	"___path.static.skin___/css/mobile.css",
	"___path.static.skin___/css/comments.css",
	"___path.static.skin___/css/profile.css",
	"___path.static.skin___/css/infobox.css",
	"___path.static.skin___/css/jquery.notifier.css",
	"___path.static.skin___/css/modals.css",
	"___path.static.skin___/css/tables.css",
	"___path.static.skin___/css/slider/nivo-slider.css",
	"___path.static.skin___/css/slider/themes/new/new.css",
	"___path.static.skin___/css/slider/themes/new_2/new_2.css",
	"___path.static.skin___/css/slider/themes/new_3/new_3.css",
	"___path.root.engine_lib___/external/jquery/markitup/skins/simple/style.css",
	"___path.root.engine_lib___/external/jquery/markitup/sets/default/style.css",
	"___path.root.engine_lib___/external/jquery/jcrop/jquery.Jcrop.css",
	"___path.root.engine_lib___/external/prettify/prettify.css",
	"___path.static.skin___/css/smoothness/jquery-ui.css",
	"___path.static.skin___/themes/___view.theme___/style.css",
	"___path.static.skin___/themes/switcher/tm-1/tm-1.css",
	"___path.static.skin___/themes/switcher/tm-2/tm-2.css",
	"___path.static.skin___/themes/switcher/tm-3/tm-3.css",
	"___path.static.skin___/themes/switcher/tm-4/tm-4.css",
	"___path.static.skin___/themes/switcher/tm-5/tm-5.css",
	"___path.static.skin___/css/slidebars.css",
	"___path.static.skin___/css/print.css",

);

/**
 * Настройка топика-фотосета // список размеров превью, которые необходимо делать при загрузке фото
 */
$config['module']['topic']['photoset']['size'] = array(
	// Slider
	array(
		'w' => 1180,
		'h' => 466,
		'crop' => true,
	),             
	// Большая картинка
	array(
		'w' => 1000,
		'h' => null,
		'crop' => false,
	),
	// Для темы tm-4
	array(
		'w' => 385,
		'h' => 226,
		'crop' => true,
	),

	// Для темы tm-3
	array(
		'w' => 870,
		'h' => 300,
		'crop' => true,
	),
	// Превьюшка
	array(
		'w' => 100,
		'h' => 65,
		'crop' => true,
	),
	// Превьюшка
	array(
		'w' => 50,
		'h' => 50,
		'crop' => true,
	)
);

// до какого размера в пикселях ужимать картинку по щирине при загрузки её в топики и комменты
$config['view']['img_resize_width'] = 1180;

return $config;
?>