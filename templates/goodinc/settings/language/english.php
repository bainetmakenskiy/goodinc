<?php

/**
* English language file.
*/
return array(
		/**
	* Header
	*/

	'goodinc_header_logo_text' => 'Good Inc',
	'goodinc_header_menu_sub_1' => 'Information',
	'goodinc_header_menu_sub_2' => 'Profile',
		'goodinc_header_menu_link_1' => 'Registration',
		'goodinc_header_menu_link_2' => 'About me',

	'goodinc_menu_link_1' => 'Menu',

	'goodinc_menu_link_sub_2' => 'Filter',

	'goodinc_menu_link_sub_1' => 'Themes',
		'goodinc_menu_link_sub_1_tm_1' => 'Theme 1',
		'goodinc_menu_link_sub_1_tm_2' => 'Theme 2',
		'goodinc_menu_link_sub_1_tm_3' => 'Theme 3',
		'goodinc_menu_link_sub_1_tm_4' => 'Theme 4',
		'goodinc_menu_link_sub_1_tm_5' => 'Theme 5',
		'goodinc_menu_link_sub_1_tm_df' => 'Default',

	'goodinc_menu_link_social_1' => 'Twitter',
	'goodinc_menu_link_social_2' => 'Facebook',
	'goodinc_menu_link_social_3' => 'RSS',
	'goodinc_menu_link_social_4' => 'Mail',

	/**
	* Разное
	*/
	'text_no_data' => 'No data',
	'goodinc_text_top' => 'The best',

	/**
	* Топики
	*/
	'topic_vote_like' => 'Like it!',
	'topic_vote_total' => 'Votes',
	'topic_vote_like_star' => 'Like it',
	'topic_author_publish' => 'Publish',

	/**
	* Топики
	*/
	'goodinc_add_favorite' => 'Favorite',

	/**
	* Блог
	*/
	'goodinc_rating' => 'Rating',

	/**
	* Подвал
	*/
	'goodinc_footer_link_1' => 'About me',
	'goodinc_footer_link_2' => 'Rules',
	'goodinc_footer_link_3' => 'Advert',
	'goodinc_footer_link_4' => 'Link',
);