<?php

/**
* Русский языковой файл.
* Содержит текстовки шаблона.
*/
return array(

	/**
	* Шапка
	*/

	'goodinc_header_logo_text' => 'Good Inc',
	'goodinc_header_menu_sub_1' => 'Информация',
	'goodinc_header_menu_sub_2' => 'Профиль',
		'goodinc_header_menu_link_1' => 'Регистрация',
		'goodinc_header_menu_link_2' => 'Обо мне',

	'goodinc_menu_link_1' => 'Меню',

	'goodinc_menu_link_sub_2' => 'Сотировать',

	'goodinc_menu_link_sub_1' => 'Оформление',
		'goodinc_menu_link_sub_1_tm_1' => 'Тема 1',
		'goodinc_menu_link_sub_1_tm_2' => 'Тема 2',
		'goodinc_menu_link_sub_1_tm_3' => 'Тема 3',
		'goodinc_menu_link_sub_1_tm_4' => 'Тема 4',
		'goodinc_menu_link_sub_1_tm_5' => 'Тема 5',
		'goodinc_menu_link_sub_1_tm_df' => 'По умолчанию',

	'goodinc_menu_link_social_1' => 'Twitter',
	'goodinc_menu_link_social_2' => 'Facebook',
	'goodinc_menu_link_social_3' => 'RSS',
	'goodinc_menu_link_social_4' => 'Mail',

	/**
	* Разное
	*/
	'text_no_data' => 'Нет данных',
	'goodinc_text_top' => 'Топ',

	/**
	* Топики
	*/
	'topic_vote_like' => 'Нравится!',
	'topic_vote_total' => 'Голосов',
	'topic_vote_like_star' => 'Плюсануть топик',
	'topic_author_publish' => 'Опубликовал',

	/**
	* Топики
	*/
	'goodinc_add_favorite' => 'В избранное',

	/**
	* Блог
	*/
	'goodinc_rating' => 'Рейтинг',

	/**
	* Подвал
	*/
	'goodinc_footer_link_1' => 'О нас',
	'goodinc_footer_link_2' => 'Правила',
	'goodinc_footer_link_3' => 'Реклама',
	'goodinc_footer_link_4' => 'Ссылка',
	'goodinc_footer_link_cp' => 'Template by <a href="http://makenskiy.com">makenskiy</a>',

);