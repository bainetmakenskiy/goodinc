    <!-- Sub menu -->
    <div class="container ">
      <div class="width-12 main-menu">

        <!-- Sub menu left -->
        <nav class="menu align_left">
          <ul class="m-menu">
              <li class="icon"><i class="fa fa-map-marker"></i></li>
              <li class="start"><a href="#" class="sb-toggle-left navbar-left">{$aLang.goodinc_menu_link_1}</a></li>
          </ul>
          {include file='nav_link.tpl'}
          {hook run='main_menu'}
        </nav>
        <!-- / Sub menu left -->

        <!-- Sub menu right -->
        <div class="sub-menu align_right">

          <ul>
              
                {include file='menu.blog.tpl'}

              {if {cfg name='view.switcher.themes'} == 1}
              <li class="bg">
                <div class="dropdown-container">
                  <a href="#" class="bt-dropdown">
                      <div class="txt">{$aLang.goodinc_menu_link_sub_1}</div>
                      <div class="ar"><i class="fa fa-chevron-down"></i></div>
                  </a>
                  <div class="dropdown">
                    <div class="dropdown-line"></div>
                      <ul class="themes">
                        <li><a class="default" href="#">{$aLang.goodinc_menu_link_sub_1_tm_df}</a></li>
                        <li id="tmeme-1"><a rel="tmeme-1" href="#">{$aLang.goodinc_menu_link_sub_1_tm_1}</a></li>
                        <li id="tmeme-2"><a rel="tmeme-2" href="#">{$aLang.goodinc_menu_link_sub_1_tm_2}</a></li>
                        <li id="tmeme-3"><a rel="tmeme-3" href="#">{$aLang.goodinc_menu_link_sub_1_tm_3}</a></li>
                        <li id="tmeme-4"><a rel="tmeme-4" href="#">{$aLang.goodinc_menu_link_sub_1_tm_4}</a></li>
                        <li id="tmeme-5"><a rel="tmeme-5" href="#">{$aLang.goodinc_menu_link_sub_1_tm_5}</a></li>
                      </ul>
                    </div>
                  </div>
              </li>
              {/if}
          </ul>

        </div>
        <!-- / Sub menu right -->

      </div>
    </div>
    <!-- / Sub menu -->