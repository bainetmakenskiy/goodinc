{if $sMenuItemSelect=='index' || $sMenuItemSelect=='blog' || $sMenuItemSelect=='log'}
				<li>
	                <div class="dropdown-container">
	                    <a href="#" class="bt-dropdown-2">
	                      <div class="txt">{$aLang.goodinc_menu_link_sub_2}</div>
	                      <div class="ar"><i class="fa fa-map-marker"></i></div>
	                    </a>
	                    <div class="dropdown-2">
	                        <div class="dropdown-line"></div>
	                          	{if $sMenuItemSelect=='index'}
									<ul>
										<li {if $sMenuSubItemSelect=='good'}class="active"{/if}><a href="{cfg name='path.root.web'}/">{$aLang.blog_menu_all_good}</a></li>
										<li {if $sMenuSubItemSelect=='new'}class="active"{/if}>
											<a href="{router page='index'}newall/" title="{$aLang.blog_menu_top_period_all}">{$aLang.blog_menu_all_new} {if $iCountTopicsNew>0}+{$iCountTopicsNew}{/if}</a>
										</li>
										<li {if $sMenuSubItemSelect=='discussed'}class="active"{/if}><a href="{router page='index'}discussed/">{$aLang.blog_menu_all_discussed}</a></li>
										<li {if $sMenuSubItemSelect=='top'}class="active"{/if}><a href="{router page='index'}top/">{$aLang.blog_menu_all_top}</a></li>
										{hook run='menu_blog_index_item'}
									</ul>
								{/if}
								{if $sMenuItemSelect=='blog'}
									<ul>
										<li {if $sMenuSubItemSelect=='good'}class="active"{/if}><a href="{$sMenuSubBlogUrl}">{$aLang.blog_menu_collective_good}</a></li>
										<li {if $sMenuSubItemSelect=='new'}class="active"{/if}>
											<a href="{$sMenuSubBlogUrl}newall/" title="{$aLang.blog_menu_top_period_all}">{$aLang.blog_menu_collective_new} {if $iCountTopicsBlogNew>0}+{$iCountTopicsBlogNew}{/if}</a>
										</li>
										<li {if $sMenuSubItemSelect=='discussed'}class="active"{/if}><a href="{$sMenuSubBlogUrl}discussed/">{$aLang.blog_menu_collective_discussed}</a></li>
										<li {if $sMenuSubItemSelect=='top'}class="active"{/if}><a href="{$sMenuSubBlogUrl}top/">{$aLang.blog_menu_collective_top}</a></li>
										{hook run='menu_blog_blog_item'}
									</ul>
								{/if}
								{if $sMenuItemSelect=='log'}
									<ul>
										<li {if $sMenuSubItemSelect=='good'}class="active"{/if}><a href="{router page='personal_blog'}">{$aLang.blog_menu_personal_good}</a></li>
										<li {if $sMenuSubItemSelect=='new'}class="active"{/if}>
											<a href="{router page='personal_blog'}newall/" title="{$aLang.blog_menu_top_period_all}">{$aLang.blog_menu_personal_new} {if $iCountTopicsPersonalNew>0} +{$iCountTopicsPersonalNew}{/if}</a>	
										</li>
										<li {if $sMenuSubItemSelect=='discussed'}class="active"{/if}><a href="{router page='personal_blog'}discussed/">{$aLang.blog_menu_personal_discussed}</a></li>
										<li {if $sMenuSubItemSelect=='top'}class="active"{/if}><a href="{router page='personal_blog'}top/">{$aLang.blog_menu_personal_top}</a></li>
										{hook run='menu_blog_log_item'}
									</ul>
								{/if}
								{if $sPeriodSelectCurrent}
									<ul>
										<li {if $sPeriodSelectCurrent=='1'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=1">{$aLang.blog_menu_top_period_24h}</a></li>
										<li {if $sPeriodSelectCurrent=='7'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=7">{$aLang.blog_menu_top_period_7d}</a></li>
										<li {if $sPeriodSelectCurrent=='30'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=30">{$aLang.blog_menu_top_period_30d}</a></li>
										<li {if $sPeriodSelectCurrent=='all'}class="active"{/if}><a href="{$sPeriodSelectRoot}?period=all">{$aLang.blog_menu_top_period_all}</a></li>
									</ul>
								{/if}
	                        </div>
	                </div>
	             </li>
{/if}