{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}

                    <li>
                      {if $oMainPhoto}
                          <a href="{$oTopic->getUrl()}">
                            <img src="{$oMainPhoto->getWebPath(385crop)}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" />
                          </a>
                      {else}
                          {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                            <a href="{$oTopic->getUrl()}">
                              <img src="{$oTopic->getPreviewImageWebPath('385crop')}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
                            </a>
                          {elseif $oTopic->getPreviewImage()}
                            <a href="{$oTopic->getUrl()}">
                              <img src="{if $smarty.foreach.topicint.iteration == 1}{$oTopic->getPreviewImageWebPath('')}{else}{$oTopic->getPreviewImageWebPath('385crop')}{/if}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
                            </a>
                          {else}
                            <a href="{$oTopic->getUrl()}">
                              <img src="{if $smarty.foreach.topicint.iteration == 1}{cfg name='path.static.skin'}/images/valli_3.jpg{else}{cfg name='path.static.skin'}/images/walle_2.jpg{/if}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
                            </a>
                          {/if}
                      {/if}
                      
                      {if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
                        {assign var="bVoteInfoShow" value=true}
                      {/if}
                        
                      <span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox{/if}">
                        <a href="#" class="go js-infobox" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" title="{$aLang.topic_vote_like}"></a>
                        <a href="#" class="go-hover js-infobox" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" title="{$aLang.topic_vote_like}"></a>
                      </span>

                      <div class="txt">
                          <a href="{$oTopic->getUrl()}"><h2>{$oTopic->getTitle()|strip_tags|truncate:20:'...'}</h2></a>
                          <p>{$oTopic->getTextShort()|strip_tags|truncate:80:'...'}</p>
                      </div>
                    </li>