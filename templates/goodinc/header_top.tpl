<!-- Header -->
<header class="wrapper theme1">
    <div class="container">
      <div class="width-12">

        <!-- Logo -->
        <div class="logo align_left">
            <a href="{cfg name="path.root.web"}">
                <div class="img-logo">
                    <i class="fa fa-google"></i>
                </div>
                <div class="triangle-down"></div>
                <div class="text">
                  {$aLang.goodinc_header_logo_text}
                </div>
            </a>
        </div>
         <!-- / Logo -->

         <!-- Top nav -->
        <div class="top-nav align_right">

            <!-- dropdown -->
            <div class="dropdown-container-3">
              <a href="#" class="bt-dropdown-3">
                <div class="txt">{$aLang.goodinc_header_menu_sub_1}</div>
                <div class="ar"><i class="fa fa-chevron-down"></i></div>
                <div class="icon"><i class="fa fa-info"></i></div>
              </a>
              <div class="dropdown-3">
                    <ul>
                          <li class="active"><a href="#"><i class="fa fa-map-marker"></i>link</a></li>
                          <li><a href="#">link</a></li>
                          <li><a href="#">link</a></li>
                          <li><a href="#">link</a></li>
                    </ul>
              </div>
            </div>
            <!-- / dropdown -->

            {hook run='userbar_nav'}

            <!-- dropdown -->
            <div class="dropdown-container-4">
              <a href="#" class="bt-dropdown-4">
                <div class="txt">{$aLang.goodinc_header_menu_sub_2}</div>
                <div class="ar"><i class="fa fa-chevron-down"></i></div>
                <div class="icon"><i class="fa fa-user"></i></div>
              </a>
              <div class="dropdown-4">
                    <ul>
                      {if $oUserCurrent}
                          <li {if $sAction=='profile' && ($aParams[0]=='whois' or $aParams[0]=='')}class="active"{/if}>
                            <a href="{$oUserCurrent->getUserWebPath()}">{if $sAction=='profile' && ($aParams[0]=='whois' or $aParams[0]=='')}<i class="fa fa-map-marker"></i>{/if}{$aLang.goodinc_header_menu_link_2}</a>
                          </li>
                          <li {if $sEvent=='add'}class="active"{/if}>
                            <a href="{router page='topic'}add/" class="write visible-phone">{if $sEvent=='add'}<i class="fa fa-map-marker"></i>{/if}{$aLang.block_create}</a>
                            <a href="{router page='topic'}add/" class="write visible-desktop" id="modal_write_show">{if $sEvent=='add'}<i class="fa fa-map-marker"></i>{/if}{$aLang.block_create}</a>
                          </li>
                          <li {if $sAction=='profile' && $aParams[0]=='favourites'}class="active"{/if}>
                            <a href="{$oUserCurrent->getUserWebPath()}favourites/topics/">{if $sAction=='profile' && $aParams[0]=='favourites'}<i class="fa fa-map-marker"></i>{/if} {$aLang.user_menu_profile_favourites}</a>
                          </li>
                          <li {if $sAction=='talk'}class="active"{/if}>
                            <a href="{router page='talk'}" {if $iUserCurrentCountTalkNew}class="new-messages"{/if} id="new_messages" title="{if $iUserCurrentCountTalkNew}{$aLang.user_privat_messages_new}{/if}">{if $sAction=='talk'}<i class="fa fa-map-marker"></i>{/if}{$aLang.user_privat_messages}{if $iUserCurrentCountTalkNew} ({$iUserCurrentCountTalkNew}){/if}</a>
                          </li>
                          <li {if $sAction=='settings'}class="active"{/if}>
                            <a href="{router page='settings'}profile/">{if $sAction=='settings'}<i class="fa fa-map-marker"></i>{/if}{$aLang.user_settings}</a>
                          </li>
                          <li {if $sMenuItemSelect=='feed'}class="active"{/if}>
                            <a href="{router page='feed'}">{if $sMenuItemSelect=='feed'}<i class="fa fa-map-marker"></i>{/if}{$aLang.userfeed_title}</a>
                          </li>
                          {hook run='userbar_item'}
                          <li><a href="{router page='login'}exit/?security_ls_key={$LIVESTREET_SECURITY_KEY}">{$aLang.exit}</a></li>
                      {else}
                          {hook run='userbar_item'}
                          <li class="active visible-phone"><a href="{router page='login'}"><i class="fa fa-map-marker"></i>{$aLang.user_login_submit}</a></li>
                          <li class="active visible-desktop"><a href="{router page='login'}" class="js-login-form-show"><i class="fa fa-map-marker"></i>{$aLang.user_login_submit}</a></li>
                          <li><a href="{router page='registration'}" class="js-registration-form-show">{$aLang.goodinc_header_menu_link_1}</a></li>
                      {/if}
                    </ul>
              </div>
            </div>
            <!-- / dropdown -->

        </div>
        <!-- / Top nav -->

      </div>
    </div>
</header>
<!-- Header -->