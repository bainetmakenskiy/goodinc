			{hook run='content_end'}
		</div> <!-- /Content left -->

		
		{if !$noSidebar && $sidebarPosition != 'left'}
			{include file='sidebar.tpl'}
		{/if}

	</div> <!-- /content -->


	<!-- Footer -->
    <footer class="wrapper theme1">

        <!-- Menu -->
        <div class="container">
          <div class="width-12 main-menu">
            <div class="logo align_left">
              <a href="#">
                  <div class="img-logo">
                      <i class="fa fa-google"></i>
                  </div>
                  <div class="triangle-down"></div>
                  <div class="text">
                    {$aLang.goodinc_header_logo_text}
                  </div>
              </a>
            </div>
            <nav class="menu align_right">
              <ul class="m-menu">  
                <li class="start"><a href="#" class="sb-toggle-left navbar-left">{$aLang.goodinc_menu_link_1}</a></li>
                <li class="icon"><i class="fa fa-map-marker"></i></li>
            </ul>
          {include file='nav_link.tpl'}
              {hook run='main_menu'}
            </nav>
          </div>
        </div>
        <!-- / Menu -->

        <!-- Links -->
        <div class="container links">
          <div class="width-12">
            <ul class="pages-list clearfix">
                <li><a href="#">{$aLang.goodinc_footer_link_1}</a></li>
                <li><a href="#">{$aLang.goodinc_footer_link_2}</a></li>
                <li><a href="#">{$aLang.goodinc_footer_link_3}</a></li>
                <li><a href="#">{$aLang.goodinc_footer_link_4}</a></li>
            </ul>
            <p class="margin-0">{$sHtmlDescription}</p>

            <ul class="align_right">
                <li>{hook run='copyright'}, {$aLang.goodinc_footer_link_cp}</li>
            </ul>
          </div>
        </div>
        <!-- / Links -->

        {hook run='footer_end'}
    </footer>
    <!-- / Footer -->

{include file='toolbar.tpl'}

</div>

{hook run='body_end'}

</body>
</html>