{include file='header.tpl'}

<h2 class="page-header">{$aLang.search}</h2>

{hook run='search_begin'}

		<!-- <div class="input-icon align_left">
        	<form action="{router page='search'}topics/">
	          <input type="text" value="" placeholder="{$aLang.search}" maxlength="255" name="q" class="input-width-full">
	          <div class="icon theme1">
	            <label class="search">
	              {hook run='search_form_begin'}
	              <input type="submit" value="" title="{$aLang.search_submit}">
	              {hook run='search_form_end'}
	            </label>
	          </div>
	        </form>
        </div> -->

{hook run='search_end'}

{include file='footer.tpl'}