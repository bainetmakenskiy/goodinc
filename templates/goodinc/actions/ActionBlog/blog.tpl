{include file='header.tpl'}
{assign var="oUserOwner" value=$oBlog->getOwner()}
{assign var="oVote" value=$oBlog->getVote()}


<script type="text/javascript">
	jQuery(function($){
		ls.lang.load({lang_load name="blog_fold_info,blog_expand_info"});
	});
</script>	

		<div class="blog">
			<!-- Topic tm-3 -->
            <section class="topic theme3">
                <header>
                    <a href="#"><img src="{$oBlog->getAvatarPath(870)}" class="full-with"></a>

                    {if $oUserCurrent and $oUserCurrent->getId()!=$oBlog->getOwnerId()}
                    	<a href="#" onclick="ls.blog.toggleJoin(this,{$oBlog->getId()}); return false;" class="share js-infobox" title="{if $oBlog->getUserIsJoin()}{$aLang.blog_leave}{else}{$aLang.blog_join}{/if}"><i class="fa {if $oBlog->getUserIsJoin()}fa-minus{else}fa-plus{/if}"></i></a>
                    {/if}

                    {if $oUserCurrent and ($oUserCurrent->getId()==$oBlog->getOwnerId() or $oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() )}
                    	<a href="{router page='blog'}edit/{$oBlog->getId()}/" title="{$aLang.blog_edit}" class="share js-infobox">{$aLang.blog_edit}</a></li>
                    	{if $oUserCurrent->isAdministrator()}
							<a href="#" title="{$aLang.blog_delete}" id="blog_delete_show" class="share del js-infobox">{$aLang.blog_delete}</a>
						{else}
							<a href="{router page='blog'}delete/{$oBlog->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" class="share del js-infobox" title="{$aLang.blog_delete}" onclick="return confirm('{$aLang.blog_admin_delete_confirm}');" >{$aLang.blog_delete}</a>
						{/if}
                    {/if}
                </header>
                <section class="text">
                  <a href="#" class="title"><h2>{if $oBlog->getType()=='close'}<i title="{$aLang.blog_closed}" class="fa fa-lock"></i> {/if}{$oBlog->getTitle()|escape:'html'}</h2></a>

                 	{$oBlog->getDescription()}

                 	
                  <a href="#" class="cut" id="blog-more" onclick="return ls.blog.toggleInfo()">{$aLang.blog_expand_info}</i></a>

                  <div class="blog-more-content" id="blog-more-content" style="display: none;">
					<div class="blog-content">
						<p class="blog-description"></p>
					</div>
					
					
					<div class="blog-footer">
						{hook run='blog_info_begin' oBlog=$oBlog}
						<strong>{$aLang.blog_user_administrators} ({$iCountBlogAdministrators}):</strong>							
						<a href="{$oUserOwner->getUserWebPath()}" class="user"><i class="fa fa-user"></i>{$oUserOwner->getLogin()}</a>
						{if $aBlogAdministrators}			
							{foreach from=$aBlogAdministrators item=oBlogUser}
								{assign var="oUser" value=$oBlogUser->getUser()}  									
								<a href="{$oUser->getUserWebPath()}" class="user"><i class="fa fa-user"></i>{$oUser->getLogin()}</a>
							{/foreach}	
						{/if}<br />		

						
						<strong>{$aLang.blog_user_moderators} ({$iCountBlogModerators}):</strong>
						{if $aBlogModerators}						
							{foreach from=$aBlogModerators item=oBlogUser}  
								{assign var="oUser" value=$oBlogUser->getUser()}									
								<a href="{$oUser->getUserWebPath()}" class="user"><i class="icon-user"></i>{$oUser->getLogin()}</a>
							{/foreach}							
						{else}
							{$aLang.blog_user_moderators_empty}
						{/if}<br />
						
						
						<strong>{$aLang.blog_user_readers} ({$iCountBlogUsers}):</strong>
						{if $aBlogUsers}
							{foreach from=$aBlogUsers item=oBlogUser}
								{assign var="oUser" value=$oBlogUser->getUser()}
								<a href="{$oUser->getUserWebPath()}" class="user"><i class="fa fa-user"></i>{$oUser->getLogin()}</a>
							{/foreach}
							
							{if count($aBlogUsers) < $iCountBlogUsers}
								<br /><a href="{$oBlog->getUrlFull()}users/">{$aLang.blog_user_readers_all}</a>
							{/if}
						{else}
							{$aLang.blog_user_readers_empty}
						{/if}
						{hook run='blog_info_end' oBlog=$oBlog}
					</div>
				  </div>

				{hook run='blog_info' oBlog=$oBlog}

                </section>
                <footer id="vote_area_blog_{$oBlog->getId()}" >
                  <ul>
                      <li id="vote_total_blog_{$oBlog->getId()}">{$aLang.goodinc_rating}: {if $oBlog->getRating() > 0}+{/if}{$oBlog->getRating()}</li>
                      <li class="vote"><span>{$aLang.blog_vote_count}: {$oBlog->getCountVote()}</span><a href="#" onclick="return ls.vote.vote({$oBlog->getId()},this,1,'blog');"><i class="fa fa-heart"></i></a></li>
                  </ul>
                  <div class="line"></div>
                  <div class="topic-type">
                        <i class="fa fa fa-cloud"></i>
                  </div>
                </footer>

            </section>
            <!-- Topic tm-3 -->
        </div>

{if $oUserCurrent and $oUserCurrent->isAdministrator()}
	<div id="blog_delete_form" class="modal">
		<header class="modal-header">
			<h3>{$aLang.blog_admin_delete_title}</h3>
			<a href="#" class="close jqmClose"></a>
		</header>
		
		
		<form action="{router page='blog'}delete/{$oBlog->getId()}/" method="POST" class="modal-content">
			<p><label for="topic_move_to">{$aLang.blog_admin_delete_move}:</label>
			<select name="topic_move_to" id="topic_move_to" class="input-width-full">
				<option value="-1">{$aLang.blog_delete_clear}</option>
				{if $aBlogs}
					<optgroup label="{$aLang.blogs}">
						{foreach from=$aBlogs item=oBlogDelete}
							<option value="{$oBlogDelete->getId()}">{$oBlogDelete->getTitle()|escape:'html'}</option>
						{/foreach}
					</optgroup>
				{/if}
			</select></p>
			
			<input type="hidden" value="{$LIVESTREET_SECURITY_KEY}" name="security_ls_key" />
			<button type="submit" class="button button-primary">{$aLang.blog_delete}</button>
		</form>
	</div>
{/if}


{if $bCloseBlog}
	{$aLang.blog_close_show}
{else}
	{include file='topic_list.tpl'}
{/if}


{include file='footer.tpl'}