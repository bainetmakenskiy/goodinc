{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}
{assign var="oFavourite" value=$oTopic->getFavourite()}

{if $bTopicList}

	{include file='topic_theme.tpl'}

{else}

    {include file='topic_part_header.tpl'}

		{hook run='topic_content_begin' topic=$oTopic bTopicList=$bTopicList}
		
			{$oTopic->getText()}
		
		{hook run='topic_content_end' topic=$oTopic bTopicList=$bTopicList}

	{include file='topic_part_footer.tpl'}

{/if}