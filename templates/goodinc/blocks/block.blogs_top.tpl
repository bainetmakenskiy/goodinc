				<ul>
				{foreach from=$aBlogs item=oBlog}
                    <li>
                        <a href="{$oBlog->getUrlFull()}"><strong>{if $oBlog->getType()=='close'}<i title="{$aLang.blog_closed}" class="fa fa-lock"></i>{/if} {$oBlog->getTitle()|escape:'html'}</strong> <span>({$aLang.blog_rating}: {$oBlog->getRating()})</span></a> 
                    </li>
                {/foreach}
                </ul>			