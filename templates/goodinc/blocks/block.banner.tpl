            <!-- Block -->
            <section class="block theme1 margin-20">
                <header>
                  <a href="#" class="go js-infobox" title="Перейти"></a>
                  <a href="#" class="go-hover js-infobox" title="Перейти"></a>
                    <a href="#"><img src="{cfg name="path.static.skin"}/images/block_theme1.jpg" alt="" title="Перейти" class="full-with" /></a>
                </header>
                <div class="txt">
                    <a href="#" title="Перейти" class="title"><h5>Заголовок баннера</h5></a>
                    <p>Какой-то рекламный текст... Какой-то рекламный текст... Какой-то рекламный текст...</p>
                </div>
            </section>
            <!-- / Block -->