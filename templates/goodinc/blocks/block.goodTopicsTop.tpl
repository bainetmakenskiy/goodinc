{if $goodtpl_aTopicsTop and count($goodtpl_aTopicsTop)}
    <!-- Slider -->
    <div class="container">
      <div id="wrapper" class="width-12">
        <div class="slider-wrapper 
        {if {cfg name='view.topic.slider_theme'} == 'tm-1'}
          theme-new
        {elseif {cfg name='view.topic.slider_theme'} == 'tm-2'}
          theme-new-2
        {elseif {cfg name='view.topic.slider_theme'} == 'tm-3'}
          theme-new-3
        {/if}
        ">
            <div id="slider" class="nivoSlider">

                {foreach from=$goodtpl_aTopicsTop item=oTopic}
                  {assign var="oUser" value=$oTopic->getUser()}
                    {assign var=oMainPhoto value=$oTopic->getPhotosetMainPhoto()}
                      {if $oMainPhoto}
                        <a href="{$oTopic->getUrl()}">
                          <img src="{$oMainPhoto->getWebPath(1180crop)}" data-thumb="{$oMainPhoto->getWebPath(385crop)}" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" id="photoset-main-image-{$oTopic->getId()}" />
                        </a>
                      {else}
                        {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
                          <a href="{$oTopic->getUrl()}">
                            <img src="{$oTopic->getPreviewImageWebPath('1180crop')}" data-thumb="{$oTopic->getPreviewImageWebPath('385crop')}" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" />
                          </a>
                        {elseif $oTopic->getPreviewImage()}
                          <a href="{$oTopic->getUrl()}">
                            <img src="{$oTopic->getPreviewImageWebPath('1180crop')}" data-thumb="{$oTopic->getPreviewImageWebPath('385crop')}" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" />
                          </a>
                        {else}
                          <a href="{$oTopic->getUrl()}">
                            <img src="{cfg name='path.static.skin'}/images/topic_avatar_1180x466.png" data-thumb="{cfg name='path.static.skin'}/images/topic_avatar_420x246.png" alt="{$oTopic->getTitle()|escape:'html'}" title="#{$oTopic->getId()}" />
                          </a>
                        {/if}
                      {/if}
                {/foreach}
            </div>


            {foreach from=$goodtpl_aTopicsTop item=oTopic}
                  {assign var="oUser" value=$oTopic->getUser()}
                  <div id="{$oTopic->getId()}" class="nivo-html-caption">
                      <h2>{$aLang.goodinc_text_top}: {$oTopic->getTitle()|escape:'html'}</h2>
                      <p>{$oTopic->getTextShort()|strip_tags|truncate:100:'...'}</p>
                      <div class="triangle-down"></div>
                      <div class="panel">
                          <a href="{$oTopic->getUrl()}"><i class="fa fa-plus"></i></a>
                          <time><i class="fa fa-clock-o"></i>1 Nov 2014</time>
                      </div>
                  </div>
            {/foreach}

        </div>
      </div>
    </div>
    <!-- / Slider -->
{/if}