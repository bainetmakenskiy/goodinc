            <!-- Block -->
			<section class="block theme2">

				{hook run='block_stream_nav_item' assign="sItemsHook"}
				
				<div class="block-content">
					<div class="js-block-stream-content">
						{$sStreamComments}
					</div>
				</div>
			</section>
			<!-- / Block -->

