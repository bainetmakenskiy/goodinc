				<ul>
					{foreach from=$aComments item=oComment name="cmt"}
						{assign var="oUser" value=$oComment->getUser()}
						{assign var="oTopic" value=$oComment->getTarget()}
						{assign var="oBlog" value=$oTopic->getBlog()}
                   
		                    <li>
		                        <a href="{if $oConfig->GetValue('module.comment.nested_per_page')}{router page='comments'}{else}{$oTopic->getUrl()}#comment{/if}{$oComment->getId()}" class="title"><h5>{$oTopic->getTitle()|escape:'html'}</h5></a>
		                        <p>{$oComment->getText()|strip_tags|trim|truncate:100:'...'|escape:'html'}</p>
		                        <div class="panel">
		                            <a href="{if $oConfig->GetValue('module.comment.nested_per_page')}{router page='comments'}{else}{$oTopic->getUrl()}#comment{/if}{$oComment->getId()}"><i class="fa fa-plus"></i> <time datetime="{date_format date=$oComment->getDate() format='c'}">{date_format date=$oComment->getDate() hours_back="12" minutes_back="60" now="60" day="day H:i" format="j F Y, H:i"}</time></a>
		                        </div>
		                    </li>
                    
                    {/foreach}
                </ul>