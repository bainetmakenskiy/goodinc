<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="ru"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="ru"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="ru"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->
<head>
	{hook run='html_head_begin'}

	<meta charset="utf-8">
	<title>{$sHtmlTitle}</title>
	<meta name="description" content="{$sHtmlDescription}">
	<meta name="keywords" content="{$sHtmlKeywords}">
	<meta name="author" content="Makenskiy Victor makenskiy.com">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	{$aHtmlHeadFiles.css}

	<!-- Поддержка html 5 тегов для IE -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="{cfg name='path.static.skin'}/images/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{cfg name='path.static.skin'}/images/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{cfg name='path.static.skin'}/images/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{cfg name='path.static.skin'}/images/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="{cfg name='path.static.skin'}/images/apple-touch-icon-57-precomposed.png">

  <link rel="search" type="application/opensearchdescription+xml" href="{router page='search'}opensearch/" title="{cfg name='view.name'}" />

	{if $aHtmlRssAlternate}
		<link rel="alternate" type="application/rss+xml" href="{$aHtmlRssAlternate.url}" title="{$aHtmlRssAlternate.title}">
	{/if}

	{if $sHtmlCanonical}
		<link rel="canonical" href="{$sHtmlCanonical}" />
	{/if}
	
	{if $bRefreshToHome}
		<meta  HTTP-EQUIV="Refresh" CONTENT="3; URL={cfg name='path.root.web'}/">
	{/if}
	
	<script type="text/javascript">
		var DIR_WEB_ROOT 			= '{cfg name="path.root.web"}';
		var DIR_STATIC_SKIN 		= '{cfg name="path.static.skin"}';
		var DIR_ROOT_ENGINE_LIB 	= '{cfg name="path.root.engine_lib"}';
		var LIVESTREET_SECURITY_KEY = '{$LIVESTREET_SECURITY_KEY}';
		var SESSION_ID				= '{$_sPhpSessionId}';
		var BLOG_USE_TINYMCE		= '{cfg name="view.tinymce"}';
		
		var TINYMCE_LANG = 'en';
		{if $oConfig->GetValue('lang.current') == 'russian'}
			TINYMCE_LANG = 'ru';
		{/if}

		var aRouter = new Array();
		{foreach from=$aRouter key=sPage item=sPath}
			aRouter['{$sPage}'] = '{$sPath}';
		{/foreach}
	</script>
	
	
	{$aHtmlHeadFiles.js}

	
	<script type="text/javascript">
		var tinyMCE = false;
		ls.lang.load({json var = $aLangJs});
		ls.registry.set('comment_max_tree',{json var=$oConfig->Get('module.comment.max_tree')});
		ls.registry.set('block_stream_show_tip',{json var=$oConfig->Get('block.stream.show_tip')});
	</script>
	
	
	{hook run='html_head_end'}
</head>



{if $oUserCurrent}
	{assign var=body_classes value=$body_classes|cat:' ls-user-role-user'}
	
	{if $oUserCurrent->isAdministrator()}
		{assign var=body_classes value=$body_classes|cat:' ls-user-role-admin'}
	{/if}
{else}
	{assign var=body_classes value=$body_classes|cat:' ls-user-role-guest'}
{/if}

{if !$oUserCurrent or ($oUserCurrent and !$oUserCurrent->isAdministrator())}
	{assign var=body_classes value=$body_classes|cat:' ls-user-role-not-admin'}
{/if}

{add_block group='toolbar' name='toolbar_admin.tpl' priority=100}
{add_block group='toolbar' name='toolbar_scrollup.tpl' priority=-100}




<body class="{$body_classes}">
	{hook run='body_begin'}


<div class="sb-slidebar sb-left">
  {include file='nav_link.tpl'}
</div>

<div id="sb-site">	
	{if $oUserCurrent}
		{include file='window_write.tpl'}
		{include file='window_favourite_form_tags.tpl'}
	{else}
		{include file='window_login.tpl'}
	{/if}
	
	{hook run='container_class'}


	{include file='header_top.tpl'}


	{include file='nav.tpl'}

	<!-- search &  social -->
    <div class="container search">
      <div class="width-12">

        <!-- Search -->
        <div class="input-icon align_left">
        	<form action="{router page='search'}topics/">
	          <input type="text" value="" placeholder="{$aLang.search}" name="q" maxlength="255" class="input-width-3">
	          <div class="icon theme1"><!-- <i class="fa fa-search"></i> -->
	            <label class="search">
	              <input type="submit" value="" title="{$aLang.search_submit}" class="js-infobox">
	            </label>
	          </div>
	        </form>
        </div>
        <!-- / Search -->

        <!-- Social -->
        <div class="social align_right">
            <ul>
                <li><a href="#"><i class="fa fa-twitter"></i> {$aLang.goodinc_menu_link_social_1}</a></li>
                <li><a href="#"><i class="fa fa-facebook"></i> {$aLang.goodinc_menu_link_social_2}</a></li>
                <li><a href="{router page='rss'}"><i class="fa fa-rss"></i> {$aLang.goodinc_menu_link_social_3}</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> {$aLang.goodinc_menu_link_social_4}</a></li>
            </ul>
        </div>
        <!-- Social -->

      </div>
    </div>
    <!-- / search &  social -->
	
	{if ($sAction=='index' and $sEvent=='newall') || ($sAction=='index' and $sEvent=='new') || ($sAction=='index' and $sEvent=='discussed') || ($sAction=='index' and $sEvent=='top') }
			{* Вырезаем слайдер топ с главной, оставляя только топики на страницах: newall, new, discussed, top *}
		{else}
			{if $sAction=='index' && {cfg name='view.topic.slider'} == 1}
           		{insert name="block" block=goodTopicsTop}
      		{/if}
    {/if}
    
    {if ($sAction=='index' and $sEvent=='newall') || ($sAction=='index' and $sEvent=='new') || ($sAction=='index' and $sEvent=='discussed') || ($sAction=='index' and $sEvent=='top') }
			{* Вырезаем слайдер топ с главной, оставляя только топики на страницах: newall, new, discussed, top *}
		{else}
			{if $sAction=='index' && {cfg name='view.carousel.index'} == 1}
		    <!-- Carousel -->
		    <div class="wrapper">
		      <div class="container slyle-1">
		      <div class="slider-2 width-12">
		          <div class="list_carousel responsive">
		          	{if {cfg name='view.carousel.type'} == 'people'}
		          			<ul id="slider-2">
		          				{insert name="block" block=goodUsersTop}
		          			</ul>
		          	{elseif {cfg name='view.carousel.type'} == 'new'}
		          		{assign var="aTopics" value=$LS->Topic_GetTopicsNew(1,{cfg name='view.carousel.topic_n'})}
		                {assign var="aTopics" value=$aTopics.collection}
			                <ul id="slider-2">
			                {foreach from=$aTopics item=oTopic}
			                        {include file="topic_carousel.tpl"}                         
			                {/foreach}
			              	</ul>
			        {elseif {cfg name='view.carousel.type'} == 'blog'}
			        	{assign var="aTopics" value=$LS->Topic_GetTopicsByBlogId({cfg name='view.carousel.blog_n'}, 1, 10, array ('blog'), false)}
						{assign var="aTopics" value=$aTopics.collection}
			        	<ul id="slider-2">
			        		{foreach from=$aTopics item=oTopic}
    							{include file="topic_carousel.tpl"} 
							{/foreach}
			        	</ul>
		            {/if}
		          </div>
		          <div class="slider-nav">
		            <a id="prev2" class="prev" href="#"></a>
		            <a id="next2" class="next" href="#"></a>
		          </div>
		      </div>
		      </div>
		    </div>
		    <!-- / Carousel -->
		{/if}
    {/if}
     
    <!-- content -->
    <div class="container 
    {if ($sAction=='index' and $sEvent=='newall') || ($sAction=='index' and $sEvent=='new') || ($sAction=='index' and $sEvent=='discussed') || ($sAction=='index' and $sEvent=='top') || {cfg name='view.carousel.index'} == 0}
			{* Вырезаем слайдер топ с главной, оставляя только топики на страницах: newall, new, discussed, top *}
	{else}
			{if $sAction=='index'}
    			content-margin
    		{/if}
    {/if}

    {hook run='container_class'}" role="main" {if $sMenuItemSelect=='profile'}itemscope itemtype="http://data-vocabulary.org/Person"{/if}>

    	<!-- Content left -->
        <div class="content {if $noSidebar}width-12{else}two-thirds{/if} {hook run='wrapper_class'}"><!-- If not sidebar, delete class [two-thirds], and add class [width-12]. -->
    	
    	{include file='nav_content.tpl'}
		{include file='system_message.tpl'}
				
				{hook run='content_begin'}