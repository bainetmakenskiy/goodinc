{assign var="oBlog" value=$oTopic->getBlog()}
{assign var="oUser" value=$oTopic->getUser()}
{assign var="oVote" value=$oTopic->getVote()}

	   		<!-- Topic -->
            <article class="topic-content topic theme2 text js-topic">
                <header>
                	<ul class="topic-tags align_left js-favourite-insert-after-form js-favourite-tags-topic-{$oTopic->getId()}">
						<li><a href="{$oBlog->getUrlFull()}" class="category">{$oBlog->getTitle()|escape:'html'} <i class="fa fa-folder-open-o"></i></a></li>
						
						{strip}
							{if $oTopic->getTagsArray()}
								{foreach from=$oTopic->getTagsArray() item=sTag name=tags_list}
									<li><a rel="tag" href="{router page='tag'}{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a> {if !$smarty.foreach.tags_list.first}, {/if}</li>
								{/foreach}
							{else}
								<li>{$aLang.topic_tags_empty}</li>
							{/if}
							
							{if $oUserCurrent}
								{if $oFavourite}
									{foreach from=$oFavourite->getTagsArray() item=sTag name=tags_list_user}
										<li class="topic-tags-user js-favourite-tag-user"><a rel="tag" href="{$oUserCurrent->getUserWebPath()}favourites/topics/tag/{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a> ,</li>
									{/foreach}
								{/if}
								
								<li class="topic-tags-edit js-favourite-tag-edit" {if !$oFavourite}style="display:none;"{/if}>
									<a href="#" onclick="return ls.favourite.showEditTags({$oTopic->getId()},'topic',this);" class="link-dotted">{$aLang.favourite_form_tags_button_show}</a>
								</li>
							{/if}
						{/strip}

						 <li><i class="fa fa-tags"></i></li>
						 
					</ul>

					<ul class="actions">
						{if $oUserCurrent and ($oUserCurrent->getId()==$oTopic->getUserId() or $oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() or $oBlog->getUserIsModerator() or $oBlog->getOwnerId()==$oUserCurrent->getId())}
							<li><a href="{cfg name='path.root.web'}/{$oTopic->getType()}/edit/{$oTopic->getId()}/" title="{$aLang.topic_edit}" class="actions-edit">{$aLang.topic_edit}</a></li>
						{/if}
						
						{if $oUserCurrent and ($oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() or $oBlog->getOwnerId()==$oUserCurrent->getId())}
							<li><a href="{router page='topic'}delete/{$oTopic->getId()}/?security_ls_key={$LIVESTREET_SECURITY_KEY}" title="{$aLang.topic_delete}" onclick="return confirm('{$aLang.topic_delete_confirm}');" class="actions-delete">{$aLang.topic_delete}</a></li>
						{/if}

						{if $oUserCurrent and ($oUserCurrent->getId()==$oTopic->getUserId() or $oUserCurrent->isAdministrator() or $oBlog->getUserIsAdministrator() or $oBlog->getUserIsModerator() or $oBlog->getOwnerId()==$oUserCurrent->getId())}
						<li><i class="fa fa-cogs"></i></li>
						{/if}
					</ul>

                    
                </header>
                <div class="line"></div>
                <section class="text">
                  <a href="{$oTopic->getUrl()}" class="title">
                  	<h1>
                  		{if $oTopic->getPublish() == 0}   
							<i class="fa fa-lock js-infobox" title="{$aLang.topic_unpublish}"></i>
						{/if}
						
						{if $oTopic->getType() == 'link'} 
							<i class="fa fa-external-link js-infobox" title="{$aLang.topic_link}"></i>
						{/if}

						{$oTopic->getTitle()|escape:'html'}
                  	</h1>
                  </a>

                    <a href="#" onclick="return ls.favourite.toggle({$oTopic->getId()},this,'topic');" class="share" id="fav_count_topic_{$oTopic->getId()}">
                   	{if $oUserCurrent && $oTopic->getIsFavourite()}
                    	{$oTopic->getCountFavourite()}
                    {else}
                    	<i class="fa fa-plus"></i>
                   	{/if}
                   </a>