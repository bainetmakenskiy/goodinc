	{if {cfg name='view.topic.theme'} == 'tm-1'}
	{* Тема топика 1 *}

			<!-- Topic tm-1 -->
            <section class="topic theme1 js-topic">
                <header>
                    <a href="{$oTopic->getUrl()}"><h2>{$oTopic->getTitle()|escape:'html'}</h2></a>
                        <ul class="panel">
                            <li class="first">
                            	<time datetime="{date_format date=$oTopic->getDateAdd() format='c'}" title="{date_format date=$oTopic->getDateAdd() format='j F Y, H:i'}">
									{date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}
								</time>
                            </li>
							<li>
								<a href="{$oTopic->getUrl()}#comments" title="{$aLang.topic_comment_read}">{$oTopic->getCountComment()} {$oTopic->getCountComment()|declension:$aLang.comment_declension:'russian'}</a>
									{if $oTopic->getCountCommentNew()}<span>+{$oTopic->getCountCommentNew()}</span>{/if}
							</li>
                            <li>
                            	{if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
									{assign var="bVoteInfoShow" value=true}
								{/if}

								
                                <ul class="star" title="{$aLang.topic_vote_like_star}">	
	                                    <li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 1}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    <li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 2}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    <li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 3}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    <li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 4}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    <li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 5}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    {if $oTopic->getRating() >= 6}
	                                    	<li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 6}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    {/if}
	                                    {if $oTopic->getRating() >= 7}
	                                    	<li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 7}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    {/if}
	                                    {if $oTopic->getRating() >= 8}
	                                    	<li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 8}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    {/if}
	                                    {if $oTopic->getRating() >= 9}
	                                    	<li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 9}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    {/if}
	                                    {if $oTopic->getRating() >= 10}
	                                    	<li><a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" {if $oTopic->getRating() >= 10}class="active"{/if}><i class="fa fa-star"></i></a></li>
	                                    {/if}
                                </ul>


                              	{$aLang.topic_vote_total} {if $oTopic->getRating() > 0}+{/if}{$oTopic->getRating()}

                              
		                      	<span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox-vote-topic{/if}">
										
		                      	</span>
		                      	<a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" class="like">{$aLang.topic_vote_like}</a>

		                      	{if $bVoteInfoShow}
									<div id="vote-info-topic-{$oTopic->getId()}" style="display: none;">
										+ {$oTopic->getCountVoteUp()}<br/>
										- {$oTopic->getCountVoteDown()}<br/>
										&nbsp; {$oTopic->getCountVoteAbstain()}<br/>
										{hook run='topic_show_vote_stats' topic=$oTopic}
									</div>
								{/if}
                            </li>
                        </ul>
                </header>
                <section class="text">
                    <div class="topic-avatar">
                      	{if $oMainPhoto}
	                        <a href="{$oTopic->getUrl()}">
	                          <img src="{$oMainPhoto->getWebPath(870crop)}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" />
	                        </a>
		                {else}
		                        {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
		                          <a href="{$oTopic->getUrl()}">
		                            <img src="{$oTopic->getPreviewImageWebPath('870crop')}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
		                          </a>
		                        {elseif $oTopic->getPreviewImage()}
		                          <a href="{$oTopic->getUrl()}">
		                            <img src="{$oTopic->getPreviewImageWebPath('870crop')}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
		                          </a>
		                        {else}
		                          <a href="{$oTopic->getUrl()}">
		                            <img src="{cfg name='path.static.skin'}/images/topic_avatar_870x300.jpg" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
		                          </a>
		                        {/if}
		                {/if}
                    </div>

                    {$oTopic->getTextShort()}
						
						{if $oTopic->getTextShort()!=$oTopic->getText()}
							<br/>
							<a href="{$oTopic->getUrl()}#cut" class="cut" title="{$aLang.topic_read_more}">
								{if $oTopic->getCutText()}
									{$oTopic->getCutText()}
								{else}
									{$aLang.topic_read_more}
								{/if}
								<i class="fa fa-arrow-right"></i>
							</a>
						{/if}

                </section>
                <div class="topic-type">
                        <i class="fa 
                        {if $oTopic->getType() == 'link'}
                        	fa-external-link
                        {elseif $oTopic->getType() == 'photoset'}
                        	fa-file-image-o
                        {elseif $oTopic->getType() == 'question'}
                        	fa-question-circle
                        {else}
                        	fa-file-text-o
                        {/if}
                        "></i>
                    </div>
                <footer>
                    <ul class="topic-tags js-favourite-insert-after-form js-favourite-tags-topic-{$oTopic->getId()}">
						<li><a href="{$oBlog->getUrlFull()}" class="category">{$oBlog->getTitle()|escape:'html'} <i class="fa fa-folder-open-o"></i></a></li>
						
						{strip}
							{if $oTopic->getTagsArray()}
								{foreach from=$oTopic->getTagsArray() item=sTag name=tags_list}
									<li><a rel="tag" href="{router page='tag'}{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a> {if !$smarty.foreach.tags_list.first}, {/if}</li>
								{/foreach}
							{else}
								<li>{$aLang.topic_tags_empty}</li>
							{/if}
							
							{if $oUserCurrent}
								{if $oFavourite}
									{foreach from=$oFavourite->getTagsArray() item=sTag name=tags_list_user}
										<li class="topic-tags-user js-favourite-tag-user"><a rel="tag" href="{$oUserCurrent->getUserWebPath()}favourites/topics/tag/{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a> ,</li>
									{/foreach}
								{/if}
								
								<li class="topic-tags-edit js-favourite-tag-edit" {if !$oFavourite}style="display:none;"{/if}>
									<a href="#" onclick="return ls.favourite.showEditTags({$oTopic->getId()},'topic',this);" class="link-dotted">{$aLang.favourite_form_tags_button_show}</a>
								</li>
							{/if}
						{/strip}

						 <li><i class="fa fa-tags"></i></li>
					</ul>
                    
                </footer>
            </section>
            <!-- / Topic tm-1 -->

	{elseif {cfg name='view.topic.theme'} == 'tm-2'}
	{* Тема топика 2 *}

			<!-- Topic tm-2 -->
            <section class="topic theme2 js-topic">
                <header>
                	<ul class="topic-tags js-favourite-insert-after-form js-favourite-tags-topic-{$oTopic->getId()}">
						<li><a href="{$oBlog->getUrlFull()}" class="category">{$oBlog->getTitle()|escape:'html'} <i class="fa fa-folder-open-o"></i></a></li>
						
						{strip}
							{if $oTopic->getTagsArray()}
								{foreach from=$oTopic->getTagsArray() item=sTag name=tags_list}
									<li><a rel="tag" href="{router page='tag'}{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a> {if !$smarty.foreach.tags_list.first}, {/if}</li>
								{/foreach}
							{else}
								<li>{$aLang.topic_tags_empty}</li>
							{/if}
							
							{if $oUserCurrent}
								{if $oFavourite}
									{foreach from=$oFavourite->getTagsArray() item=sTag name=tags_list_user}
										<li class="topic-tags-user js-favourite-tag-user"><a rel="tag" href="{$oUserCurrent->getUserWebPath()}favourites/topics/tag/{$sTag|escape:'url'}/">{$sTag|escape:'html'}</a> ,</li>
									{/foreach}
								{/if}
								
								<li class="topic-tags-edit js-favourite-tag-edit" {if !$oFavourite}style="display:none;"{/if}>
									<a href="#" onclick="return ls.favourite.showEditTags({$oTopic->getId()},'topic',this);" class="link-dotted">{$aLang.favourite_form_tags_button_show}</a>
								</li>
							{/if}
						{/strip}

						 <li><i class="fa fa-tags"></i></li>
					</ul>

                    
                </header>
                <div class="line"></div>
                <section class="text">
                  <a href="{$oTopic->getUrl()}" class="title"><h2>{$oTopic->getTitle()|escape:'html'}</h2></a>
                    <a href="#" onclick="return ls.favourite.toggle({$oTopic->getId()},this,'topic');" class="share" id="fav_count_topic_{$oTopic->getId()}">
                   	{if $oUserCurrent && $oTopic->getIsFavourite()}
                    	{$oTopic->getCountFavourite()}
                    {else}
                    	<i class="fa fa-plus"></i>
                   	{/if}
                   </a>

                  	{$oTopic->getTextShort()}
						
						{if $oTopic->getTextShort()!=$oTopic->getText()}
							<br/>
							<a href="{$oTopic->getUrl()}#cut" class="cut" title="{$aLang.topic_read_more}">
								{if $oTopic->getCutText()}
									{$oTopic->getCutText()}
								{else}
									{$aLang.topic_read_more}
								{/if}
								<i class="fa fa-arrow-right"></i>
							</a>
					{/if}

                  	<a href="#" onclick="return ls.favourite.toggle({$oTopic->getId()},this,'topic');" class="share" id="fav_count_topic_{$oTopic->getId()}">
                    	{if $oUserCurrent && $oTopic->getIsFavourite()}
                    		{$oTopic->getCountFavourite()}
                    	{else}
                    		<i class="fa fa-plus"></i>
                    	{/if}
                    </a>
                </section>
                <footer>
                  <ul>
                      <li>
                      	<time datetime="{date_format date=$oTopic->getDateAdd() format='c'}" title="{date_format date=$oTopic->getDateAdd() format='j F Y, H:i'}">
							{date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}
						</time>
                      </li>
                      <li class="vote">
                      	{if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
							{assign var="bVoteInfoShow" value=true}
						{/if}
                      	<span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox-vote-topic{/if}">
								{if $oTopic->getRating() > 0}+{/if}{$oTopic->getRating()}
                      	</span>
                      	<a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" class="js-infobox" title="{$aLang.topic_vote_like}"><i class="fa fa-heart"></i></a>

                      	{if $bVoteInfoShow}
							<div id="vote-info-topic-{$oTopic->getId()}" style="display: none;">
								+ {$oTopic->getCountVoteUp()}<br/>
								- {$oTopic->getCountVoteDown()}<br/>
								&nbsp; {$oTopic->getCountVoteAbstain()}<br/>
								{hook run='topic_show_vote_stats' topic=$oTopic}
							</div>
						{/if}
                      </li>
                  </ul>
                  <div class="line"></div>
                  <div class="topic-type">
                        <i class="fa 
                        {if $oTopic->getType() == 'link'}
                        	fa-external-link
                        {elseif $oTopic->getType() == 'photoset'}
                        	fa-file-image-o
                        {elseif $oTopic->getType() == 'question'}
                        	fa-question-circle
                        {else}
                        	fa-file-text-o
                        {/if}
                        "></i>
                  </div>
                </footer>

            </section>
            <!-- / Topic tm-2 -->

	{elseif {cfg name='view.topic.theme'} == 'tm-3'}
	{* Тема топика 3 *}

			<!-- Topic tm-3 -->
            <section class="topic theme3 js-topic">
                <header>
                    {if $oMainPhoto}
	                        <a href="{$oTopic->getUrl()}">
	                          <img src="{$oMainPhoto->getWebPath(870crop)}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" />
	                        </a>
	                {else}
	                        {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{$oTopic->getPreviewImageWebPath('870crop')}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {elseif $oTopic->getPreviewImage()}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{$oTopic->getPreviewImageWebPath('870crop')}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {else}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{cfg name='path.static.skin'}/images/topic_avatar_870x300.jpg" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {/if}
	                {/if}

                    <a href="#" onclick="return ls.favourite.toggle({$oTopic->getId()},this,'topic');" class="share" id="fav_count_topic_{$oTopic->getId()}">
                    	{if $oUserCurrent && $oTopic->getIsFavourite()}
                    		{$oTopic->getCountFavourite()}
                    	{else}
                    		<i class="fa fa-plus"></i>
                    	{/if}
                    </a>
                </header>
                <section class="text">
                  <a href="{$oTopic->getUrl()}" class="title"><h2>{$oTopic->getTitle()|escape:'html'}</h2></a>

						{$oTopic->getTextShort()}
						
						{if $oTopic->getTextShort()!=$oTopic->getText()}
							<br/>
							<a href="{$oTopic->getUrl()}#cut" class="cut" title="{$aLang.topic_read_more}">
								{if $oTopic->getCutText()}
									{$oTopic->getCutText()}
								{else}
									{$aLang.topic_read_more}
								{/if}
								<i class="fa fa-arrow-right"></i>
							</a>
						{/if}

                </section>
                <footer>
                  <ul>
                      <li>
                      	<time datetime="{date_format date=$oTopic->getDateAdd() format='c'}" title="{date_format date=$oTopic->getDateAdd() format='j F Y, H:i'}">
							{date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}
						</time>
                      </li>
                      <li class="vote">
                      	{if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
							{assign var="bVoteInfoShow" value=true}
						{/if}
                      	<span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox-vote-topic{/if}">
								{if $oTopic->getRating() > 0}+{/if}{$oTopic->getRating()}
                      	</span>
                      	<a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" class="js-infobox" title="{$aLang.topic_vote_like}"><i class="fa fa-heart"></i></a>

                      	{if $bVoteInfoShow}
							<div id="vote-info-topic-{$oTopic->getId()}" style="display: none;">
								+ {$oTopic->getCountVoteUp()}<br/>
								- {$oTopic->getCountVoteDown()}<br/>
								&nbsp; {$oTopic->getCountVoteAbstain()}<br/>
								{hook run='topic_show_vote_stats' topic=$oTopic}
							</div>
						{/if}
                      </li>
                  </ul>
                  <div class="line"></div>
                  <div class="topic-type">
                        <i class="fa 
                        {if $oTopic->getType() == 'link'}
                        	fa-external-link
                        {elseif $oTopic->getType() == 'photoset'}
                        	fa-file-image-o
                        {elseif $oTopic->getType() == 'question'}
                        	fa-question-circle
                        {else}
                        	fa-file-text-o
                        {/if}
                        "></i>
                  </div>
                </footer>

            </section>
            <!-- Topic tm-3 -->

	{elseif {cfg name='view.topic.theme'} == 'tm-4'}

			<section class="topic {if $smarty.foreach.topicint.iteration == 1}theme5{else}theme4{/if} js-topic">
                      <a href="{$oTopic->getUrl()}">
                      	
                      	{if $oMainPhoto}
	                        <a href="{$oTopic->getUrl()}">
	                          <img src="{if $smarty.foreach.topicint.iteration == 1}{$oMainPhoto->getWebPath()}{else}{$oMainPhoto->getWebPath(385crop)}{/if}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" />
	                        </a>
	                    {else}
	                        {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
	                          <a href="{$oTopic->getUrl()}">
	                            <!-- <img src="{$oTopic->getPreviewImageWebPath('385crop')}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" /> -->
	                            <img src="{if $smarty.foreach.topicint.iteration == 1}{$oTopic->getPreviewImageWebPath()}{else}{$oTopic->getPreviewImageWebPath('385crop')}{/if}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {elseif $oTopic->getPreviewImage()}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{if $smarty.foreach.topicint.iteration == 1}{$oTopic->getPreviewImageWebPath('')}{else}{$oTopic->getPreviewImageWebPath('385crop')}{/if}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {else}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{if $smarty.foreach.topicint.iteration == 1}{cfg name='path.static.skin'}/images/topic_avatar_870x300.jpg{else}{cfg name='path.static.skin'}/images/topic_avatar_420x246.png{/if}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {/if}
	                    {/if}
                      </a>

                      {if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
							{assign var="bVoteInfoShow" value=true}
						{/if}
                      	
                      	<span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox{/if}">
	                      <a href="#" class="go js-infobox" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" title="{$aLang.topic_vote_like}"></a>
	                      <a href="#" class="go-hover js-infobox" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" title="{$aLang.topic_vote_like}"></a>
	                    </span>

                      <div class="txt">
                          <a href="{$oTopic->getUrl()}"><h2><strong>{$oTopic->getTitle()|escape:'html'|strip_tags|truncate:20:'...'}</strong></h2></a>
                          <p>
                          	{if $bTopicList}
                          		{if $smarty.foreach.topicint.iteration == 1}
									{$oTopic->getTextShort()}
								{else}
									{$oTopic->getTextShort()|strip_tags|truncate:90:'...'}
								{/if}
							{/if}
                          </p>
              </div>  
            </section>

        {elseif {cfg name='view.topic.theme'} == 'tm-5'}

			<section class="topic theme5 js-topic">
                      <a href="{$oTopic->getUrl()}">
                      	
                      	{if $oMainPhoto}
	                        <a href="{$oTopic->getUrl()}">
	                          <img src="{$oMainPhoto->getWebPath()}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" id="photoset-main-image-{$oTopic->getId()}" />
	                        </a>
	                    {else}
	                        {if $oTopic->getType()=='photoset' and $oTopic->getPreviewImage()}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{$oTopic->getPreviewImageWebPath()}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {elseif $oTopic->getPreviewImage()}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{$oTopic->getPreviewImageWebPath()}" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {else}
	                          <a href="{$oTopic->getUrl()}">
	                            <img src="{cfg name='path.static.skin'}/images/topic_avatar_1180x466.png" class="full-with" alt="{$oTopic->getTitle()|escape:'html'}" />
	                          </a>
	                        {/if}
	                    {/if}
                      </a>

                        {if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
							{assign var="bVoteInfoShow" value=true}
						{/if}
                      	
                      	<span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox{/if}">
	                      <a href="#" class="go js-infobox" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" title="{$aLang.topic_vote_like}"></a>
	                      <a href="#" class="go-hover js-infobox" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" title="{$aLang.topic_vote_like}"></a>
	                    </span>

                      	

                      <div class="txt">
                          <a href="{$oTopic->getUrl()}"><h2><strong>{$oTopic->getTitle()|escape:'html'}</strong></h2></a>
                          <p>
                          	{if $bTopicList}
                          		{if $smarty.foreach.topicint.iteration == 1}
									{$oTopic->getTextShort()}
								{else}
									{$oTopic->getTextShort()|strip_tags|truncate:100:'...'}
								{/if}
							{/if}
                          </p>
              </div>  
            </section>
    {/if}