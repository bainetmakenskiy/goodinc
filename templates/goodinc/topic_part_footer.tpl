	{assign var="oBlog" value=$oTopic->getBlog()}
	{assign var="oUser" value=$oTopic->getUser()}
	{assign var="oVote" value=$oTopic->getVote()}
	{assign var="oFavourite" value=$oTopic->getFavourite()}

					{if $oTopic->getType() == 'link'}
						<div class="topic-url">
							<a href="{router page='link'}go/{$oTopic->getId()}/" class="js-infobox" title="{$aLang.topic_link_count_jump}: {$oTopic->getLinkCountJump()}">{$oTopic->getLinkUrl()}</a>
						</div>
					{/if}

					<div class="topic-share" id="topic_share_{$oTopic->getId()}">
						{hookb run="topic_share" topic=$oTopic bTopicList=$bTopicList}
							<div class="yashare-auto-init" data-yashareTitle="{$oTopic->getTitle()|escape:'html'}" data-yashareLink="{$oTopic->getUrl()}" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,lj,gplus"></div>
						{/hookb}
					</div>

					<a href="#" onclick="return ls.favourite.toggle({$oTopic->getId()},this,'topic');" class="share" id="fav_count_topic_{$oTopic->getId()}">
                    	{if $oUserCurrent && $oTopic->getIsFavourite()}
                    		{$oTopic->getCountFavourite()}
                    	{else}
                    		<i class="fa fa-plus"></i>
                    	{/if}
                    </a>
                </section>
                <footer>
                  <ul>
                      <li>
                      	<time datetime="{date_format date=$oTopic->getDateAdd() format='c'}" title="{date_format date=$oTopic->getDateAdd() format='j F Y, H:i'}">
							{date_format date=$oTopic->getDateAdd() format="j F Y, H:i"}
						</time>
                      </li>
                      <li class="author">{$aLang.topic_author_publish}: <a rel="author" href="{$oUser->getUserWebPath()}">{$oUser->getLogin()}</a></li>
                      <li class="vote">
                      	{if $oVote || ($oUserCurrent && $oTopic->getUserId() == $oUserCurrent->getId()) || strtotime($oTopic->getDateAdd()) < $smarty.now-$oConfig->GetValue('acl.vote.topic.limit_time')}
							{assign var="bVoteInfoShow" value=true}
						{/if}
                      	<span id="vote_total_topic_{$oTopic->getId()}" title="{$aLang.topic_vote_count}: {$oTopic->getCountVote()}" class="{if $bVoteInfoShow}js-infobox-vote-topic{/if}">
								{if $oTopic->getRating() > 0}+{/if}{$oTopic->getRating()}
                      	</span>
                      	<a href="#" onclick="return ls.vote.vote({$oTopic->getId()},this,1,'topic');" class="js-infobox" title="{$aLang.topic_vote_like}"><i class="fa fa-heart"></i></a>

                      	{if $bVoteInfoShow}
							<div id="vote-info-topic-{$oTopic->getId()}" style="display: none;">
								+ {$oTopic->getCountVoteUp()}<br/>
								- {$oTopic->getCountVoteDown()}<br/>
								&nbsp; {$oTopic->getCountVoteAbstain()}<br/>
								{hook run='topic_show_vote_stats' topic=$oTopic}
							</div>
						{/if}
                      </li>
                  </ul>
                  <div class="line"></div>
                  <div class="topic-type">
                        <i class="fa 
                        {if $oTopic->getType() == 'link'}
                        	fa-external-link
                        {elseif $oTopic->getType() == 'photoset'}
                        	fa-file-image-o
                        {elseif $oTopic->getType() == 'question'}
                        	fa-question-circle
                        {else}
                        	fa-file-text-o
                        {/if}
                        "></i>
                  </div>
                </footer>

                {hook run='topic_show_info' topic=$oTopic}

                {if !$bTopicList}
					{hook run='topic_show_end' topic=$oTopic}
				{/if}

            </article>
            <!-- / Topic -->