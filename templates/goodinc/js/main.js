jQuery(function($){

var GOODINC = window.GOODINC || {};


/* ==================================================
// Responsive menu
================================================== */

        GOODINC.navMenu = function(){
          $.slidebars();
        }

/* ==================================================
// Dropdown Menu
================================================== */

        GOODINC.dropdownMenu = function(){
            
            /* Dropdown 1 */
            $(function() {

                var dropdown = $('.bt-dropdown');
                var menudropdown = $('.dropdown');
                
                $('ul li a', menudropdown).each(function() {
                    $(this).append('<div />');
                });
                
                dropdown.toggle(function(e) {
                    e.preventDefault();
                    menudropdown.css({display: 'block'});
                    $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
                    $(this).addClass('active');
                },function() {
                    menudropdown.css({display: 'none'});
                    $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
                    $(this).removeClass('active');
                });
                    
            });

            /* Dropdown 2 */
            $(function() {

                var dropdown = $('.bt-dropdown-2');
                var menudropdown = $('.dropdown-2');
                
                $('ul li a', menudropdown).each(function() {
                    $(this).append('<div />');
                });
                
                dropdown.toggle(function(e) {
                    e.preventDefault();
                    menudropdown.css({display: 'block'});      
                    $(this).addClass('active');
                },function() {
                    menudropdown.css({display: 'none'});  
                    $(this).removeClass('active');
                });
                    
            });

            /* Dropdown 3 */
            $(function() {

                var dropdown = $('.bt-dropdown-3');
                var menudropdown = $('.dropdown-3');
                
                $('ul li a', menudropdown).each(function() {
                    $(this).append('<div />');
                });
                
                dropdown.toggle(function(e) {
                    e.preventDefault();
                    menudropdown.css({display: 'block'});
                    $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
                    $(this).addClass('active');
                    $('.icon', this).html('<i class="fa"></i>').css({/*top: '9px'*/});
                },function() {
                    menudropdown.css({display: 'none'});
                    $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
                    $(this).removeClass('active');
                    $('.icon', this).html('<i class="fa fa-info"></i>').css({/*top: '9px'*/});
                });
                    
            });

            /* Dropdown 4 */
            $(function() {

                var dropdown = $('.bt-dropdown-4');
                var menudropdown = $('.dropdown-4');
                
                $('ul li a', menudropdown).each(function() {
                    $(this).append('<div />');
                });
                
                dropdown.toggle(function(e) {
                    e.preventDefault();
                    menudropdown.css({display: 'block'});
                    $('.ar', this).html('<i class="fa fa-chevron-up"></i>').css({/*top: '9px'*/});
                    $(this).addClass('active');
                    $('.icon', this).html('<i class="fa"></i>').css({/*top: '9px'*/});
                },function() {
                    menudropdown.css({display: 'none'});
                    $('.ar', this).html('<i class="fa fa-chevron-down"></i>').css({/*top: '9px'*/});
                    $(this).removeClass('active');
                    $('.icon', this).html('<i class="fa fa-user"></i>').css({/*top: '9px'*/});
                });
                    
            });
        }

/* ==================================================
// Responsive top slider
================================================== */

        GOODINC.nivoSlider = function(){
            $(window).load(function() {
                $('#slider').nivoSlider({
                    effect: 'random', // slideInLeft
                    animSpeed:500,
                    pauseTime: 3000,
                    controlNavThumbs: true
             
                });
             
                $('#slider').bind( 'swipeleft', function( e ) {
                    $('a.nivo-nextNav').trigger('click');
                    e.stopImmediatePropagation();
                    return false;
                 } );  
             
                 $('#slider').bind( 'swiperight', function( e ) {
                    $('a.nivo-prevNav').trigger('click');
                    e.stopImmediatePropagation();
                    return false;
                 } ); 
            });
        }

/* ==================================================
// Responsive layout, resizing the items.
================================================== */

        GOODINC.carouFredSel = function(){
            $('#slider-2').carouFredSel({
                    responsive: true,
                    auto: false,
                    width: '100%',
                    scroll: 1,
                    items: {
                        width: 400,
                        height: '400',  //  optionally resize item-height
                        visible: {
                            min: 1,
                            max: 3
                        }
                    },  
                    swipe: {
                      onMouse: true,
                      onTouch: true
                    },
                    prev: '#prev2',
                    next: '#next2'
            });
        }


/* ==================================================
// Switcher Themes
================================================== */

        GOODINC.colorThemes = function(){

            $('.default').click(function() {
                $('body').removeClass('tmeme-1 tmeme-2 tmeme-3 tmeme-4 tmeme-5').addClass('default');
                $.cookie('blind-tmeme', 'default');

                    set_tmemes();

                return false;
            });

            function set_tmemes() {
              $('body').removeClass('default tmeme-1 tmeme-2 tmeme-3 tmeme-4 tmeme-5').addClass($.cookie('blind-tmeme'));
            }

            $('.themes a').click(function() {
                tmemes = $(this).attr('rel');
                $.cookie('blind-tmeme', tmemes);
                set_tmemes();
                return false;
            });

            if (! $.cookie('blind-tmeme')) {
            $.cookie('blind-tmeme', 'default');
            }

            set_tmemes();
        }


/* ==================================================
    Init
================================================== */

    $(document).ready(function(){

        GOODINC.colorThemes();
        GOODINC.navMenu();
        GOODINC.dropdownMenu();
        GOODINC.nivoSlider();
        GOODINC.carouFredSel();

    });

/* ==================================================
    Window resizes
================================================== */
    $(window).resize(function(){
    	
		// Ather JS for .resize

    });

});
