          <ul class="m-menu-link">
              <li class="icon"><i class="fa fa-map-marker"></i></li>
              <li class="start"><a href="{cfg name='path.root.web'}/" {if $sMenuItemSelect=='index'}class="active"{/if}>{$aLang.blog_menu_all} {if $iCountTopicsNew>0}+{$iCountTopicsNew}{/if}</a></li>

              <li><a href="{router page='blog'}" {if $sMenuItemSelect=='blog'}class="active"{/if}>{$aLang.blog_menu_collective} {if $iCountTopicsCollectiveNew>0}+{$iCountTopicsCollectiveNew}{/if}</a></li>
              <li><a href="{router page='personal_blog'}" {if $sMenuItemSelect=='log'}class="active"{/if}>{$aLang.blog_menu_personal} {if $iCountTopicsPersonalNew>0}+{$iCountTopicsPersonalNew}{/if}</a></li>
              {hook run='menu_blog'}

              <li><a href="{router page='blogs'}" {if $sMenuHeadItemSelect=='blogs'}class="active"{/if}>{$aLang.blogs}</a></li>
              <li><a href="{router page='people'}" {if $sMenuHeadItemSelect=='people'}class="active"{/if}>{$aLang.people}</a></li>
              <li><a href="{router page='stream'}" {if $sMenuHeadItemSelect=='stream'}class="active"{/if}>{$aLang.stream_menu}</a></li>
              {hook run='main_menu_item'}
          </ul>